import React from "react"

import Link from "./link"
import Hours from "./hours"

import * as footerStyles from "./styles/footer.module.css"

const Footer = ({ menuLinks }) => (
  <footer>
    <div className="frame_two_columns">
      <div
        className={footerStyles.location}
        vocab="https://schema.org/"
      >
        <p className={footerStyles.address}>
          <a
            property="hasMap"
            href="https://www.schedulicity.com/scheduling/PASXM5?hl=en-US&gei=mgspZ8GDGsKs0PEPwevhwQ0"
          >
            <span className={footerStyles.title} property="name">
              Pure Alchemy Organic Salon
            </span>
            <br />
            <span property="address" typeof="PostalAddress">
              3811 South Ferdinand Street, Suite A
            </span>
            <br />
            <span property="addressLocality">Seattle</span>,{" "}
            <span property="addressRegion">WA</span>{" "}
            <span property="postalCode">98118</span>
          </a>
        </p>
        <div className={footerStyles.contact} property="member" typeof="Person">
          <p>
            <span className={footerStyles.title}>
              Contact <span property="name">Tisha</span>
            </span>
            <br />
            <a href="tel:+12065109337" property="telephone">
              206-510-9337
            </a>
            <br />
            <a href="tel:+12063958231" property="telephone">
              206-395-8231
            </a>
            <br />
            <a href="mailto:brujahairsalon@gmail.com" property="email">
              brujahairsalon@gmail.com
            </a>
            <br />
            <a href="mailto:tishaanneturner@gmail.com" property="email">
              tishaanneturner@gmail.com
            </a>
          </p>
        </div>
      </div>
      <Hours />
    </div>
    <div className="frame_one_column">
      <div className={footerStyles.sitemap}>
        <ul>
          {menuLinks.map((link) => (
            <li key={link.name}>
              <Link to={link.link}>{link.name}</Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  </footer>
)

export default Footer
