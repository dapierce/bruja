module.exports = {
  siteMetadata: {
    title: `Bruja Hair`,
    description: `Magic is in the hair in Seattle. A curly and kinky hair specialist offering cuts and high fashion color using Kevin Murphy and DevaCurl product.`,
    siteUrl: `https://www.brujasalon.com`,
    facebook: `brujahairsalon`,
    instagram: `bruja_hair`,
    siteMap: [
      {
        name: "Home",
        link: "/",
      },
      {
        name: "Stylist",
        link: "/stylists",
      },
      {
        name: "Services",
        link: "/services",
      },
      // {
      //   name: "Products",
      //   link: "/products",
      // },
      {
        name: "Book Now",
        link: "https://www.schedulicity.com/scheduling/PASXM5?hl=en-US&gei=mgspZ8GDGsKs0PEPwevhwQ0&rwg_token=AJKvS9VqfSRv7CXPl7gepzlvue1qSqHGB71mHmRjUBtsklrhN57eYCfxWC5I_qptorQOc5xBQ8XY99PdqhNaqPwLWIxOK6_U0g%3D%3D",
      },
    ],
  },
  plugins: [
    `gatsby-plugin-image`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-netlify`,
    // `gatsby-plugin-offline`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "Bruja Hair",
        short_name: "Bruja Hair",
        start_url: "/",
        background_color: "#E3C8EB",
        theme_color: "#FFFFFF",
        display: "standalone",
        icon: "src/images/icon-whitebg.png", // This path is relative to the root of the site.
        legacy: true, // this will add apple-touch-icon links to <head>
      },
    },
    `gatsby-plugin-sitemap`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    `gatsby-plugin-preact`,
  ],
}
